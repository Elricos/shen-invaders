-- chikun :: 2014
-- Loads all fonts from the /fnt folder


-- Can't do this recursively due to sizes
fnt = {
    -- Regular font
    regular = love.graphics.newFont("fnt/exo2.otf", 24),
    -- Splash screen font
    splash  = love.graphics.newFont("fnt/exo2.otf", 96),
}
