player = {}


--sets the player start position
player.set = function(x, y, w, h)

    --sets player image
    player.image = gfx.enemy1

    player.x = x
    player.y = y
    player.w = w
    player.h = h

end


--updates player every frame
player.update = function(dt)

    local xMove = 0
    if (k.isDown('left'))  then
        xMove = -1
    end
    if (k.isDown('right')) then
        xMove = xMove + 1
    end

     -- speed of horizontal movement
    player.x = math.round(player.x + 160 * dt * xMove)

    -- collision detection
    while math.overlapTable(collisions, player) do

        player.x = math.round(player.x - xMove)

    end

end
