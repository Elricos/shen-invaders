-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local play = { }


-- On state create
function play:create()

    g.setBackgroundColor(0, 0, 0)

    map.setCurrent(map.blue)

end


-- On state update
function play:update(dt)

    player.update(dt)

end


-- On state draw
function play:draw()


    g.rectangle("fill", 0, 520, 640, 40)

    map.draw()

    player.draw()

end


-- On state kill
function play:kill()

end


-- Transfer data to state loading script
return play
